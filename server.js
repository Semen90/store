var express = require('express');
var path = require('path');
var router = express.Router();  
var bodyParser = require('body-parser');
var fallback = require('express-history-api-fallback');
var os = require('os');
var fs = require('fs');

var root = __dirname + '/dist';
var app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(router);
app.use(express.static(path.join(root)));
app.use('/images',express.static(path.join(__dirname, '/public')));
app.use(fallback('index.html', { root: root }));
app.set('port', process.env.PORT || 5000);

var topTen =        fs.readFileSync('./server/data/topTen.json')
var topBanners =    fs.readFileSync('./server/data/topBanners.json')
var banners =       fs.readFileSync('./server/data/banners.json')
var collections =   fs.readFileSync('./server/data/collections.json')

router.get('/', function(req, res){
    res.sendFile(path.join(root + '/index.html'));
});

router.get('/getBannersTop', function(req, res) {
    res.send(topBanners);
    res.end();
});

router.get('/getBanners', function(req, res) {
    res.send(banners);
    res.end();
});

router.get('/getCollections', function(req, res) {
    res.send(collections);
    res.end();
});

router.get('/getTop', function(req, res) {
    res.send(topTen);
    res.end();
});

app.listen(app.get('port'), function(){
    console.log('Server start http://localhost:' + app.get('port'));
    console.log(' ');
});
























