var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var CleanWebpackPlugin = require('clean-webpack-plugin');
var UglifyJSPlugin = require('uglifyjs-webpack-plugin');

var isProd = process.env.NODE_ENV === 'production';

var prodPlugins = [
    new HtmlWebpackPlugin({
        filename: 'index.html',
        template: 'src/index.ejs'
    }),
    new CleanWebpackPlugin(['dist'], {
        dry: false            
    }),
    new UglifyJSPlugin({
        sourceMap: false
    })
]

var devPlugins = [
    new HtmlWebpackPlugin({
        filename: 'index.html',
        template: 'src/index.ejs'
    }),
    new CleanWebpackPlugin(['dist'], {
        dry: false            
    }),
]

var plugins = isProd ? prodPlugins : devPlugins;

module.exports = {
    context: __dirname,
    entry: [
        './src/index.js'
    ],
    output: {
        path: path.join(__dirname, "dist"),
        filename: 'bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/i,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['react', 'es2015', 'stage-0', 'stage-1']
                    }
                }
            },
            {
                test: /\.(scss|css)$/i,
                include: path.join(__dirname),
                use: [
                    { loader: 'style-loader' },
                    { loader: 'css-loader' },
                    { loader: 'sass-loader' }
                ]
            },
            {
                test: /\.(jpe?g|png|gif|svg|ico)$/i,
                exclude: /node_modules/,
                include: path.join(__dirname, "src", "assets", "images"),
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            context: path.join(__dirname, "src/"),
                            name: '[path][name].[ext]'
                        }
                    },
                    {
                        loader: 'image-webpack-loader',
                        options: {
                            gifsicle: {
                                interlaced: false,
                            },
                            optipng: {
                                optimizationLevel: 7,
                            },
                            pngquant: {
                                quality: '65-90',
                                speed: 4
                            },
                            mozjpeg: {
                                progressive: true,
                                quality: 65
                            }
                        }
                    }
                ]
            },
            {
                test: /\.(eot|ttf|woff|woff2|svg)$/i,
                include: path.join(__dirname, "src", "assets","fonts"),
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            context: path.join(__dirname, "src/"),
                            name: '[path][name].[ext]'
                        }
                    }
                ]
            }
        ]
    },
    plugins: plugins
}