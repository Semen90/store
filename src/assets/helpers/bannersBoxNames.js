const BANNERSBOX_NAMES = {
    popular_collections: 'popular_collections',
    you_like: 'you_like',
    classic: 'classic'
}

export default BANNERSBOX_NAMES;