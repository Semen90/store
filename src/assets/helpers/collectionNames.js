const COLLECTION_NAMES = {
    new_interesting: 'new_interesting',
    you_like: 'you_like',
    classic: 'classic'
}

export default COLLECTION_NAMES;