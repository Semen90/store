import React from 'react'
import './bannersBox.scss'

class BannersBox extends React.Component {
    constructor(props) {
        super(props)

        this.renderItems = this.renderItems.bind(this)
    }

    renderItems() {
        const itemsList = this.props.banners.banners.map((item, index) => {
            return (
                <li className="banner" key={index}>
                    <a href={item.link} className="banner__link">
                        <img src={item.icon} alt="" />
                    </a>
                </li>
            )
        })
        return itemsList;
    }

    render() {
        const p_ = this.props;
        
        if (p_.banners) {
            return (
                <div className="banners">
                    <div className="banners__top">
                        <div className="banners__name">{p_.banners.title}</div>
                    </div>
                    <ul className="banners__list">
                        {this.renderItems()}
                    </ul>
                </div>
            )
        } else {
            return (
                <div className="banners"></div>
            )
        }
        
    }
}

export default BannersBox;