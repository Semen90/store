import React from 'react';
import { Button } from 'semantic-ui-react'
import axios from 'axios'


import COLLECTION_NAMES from '../../helpers/collectionNames'
import BANNERSBOX_NAMES from '../../helpers/bannersBoxNames'
import CollectionBox from '../CollectionBox/CollectionBox'
import BannersBox from '../BannersBox/BannersBox'
import Sidebar from '../Sidebar/Sidebar'

// import Radio from '../Radio/Radio'

const classNameBtn = (currentBtn, activePlatform) => {
    if(currentBtn === activePlatform) {
        return 'primary'
    } else {
        return ''
    }

}

class MainContent extends React.Component {
    constructor() {
        super()

        this.state = {
            collections: [],
            banners: [],
            activePlatform: 'iPhone'
        }

        this.getCollection = this.getCollection.bind(this)
        // this.changePlatform = this.changePlatform.bind(this)        
    }

    componentWillMount() {
        axios.get('/getCollections')
            .then((response) => {
                this.setState( {collections: response.data} )
            })
            .catch((err) => {
                console.error(err)
            })

        axios.get('/getBanners')
            .then((response) => {
                this.setState( {banners: response.data} )
            })
            .catch((err) => {
                console.error(err)
            })
    }

    getCollection(platform, collectionName) {
        let collection;

        if( this.state.collections.iphone ) {
            if (platform == "iPhone") {
                collection = this.state.collections.iphone.find((item) => {
                    return item.name === collectionName
                })
            }

            if (platform == "iPad") {
                collection = this.state.collections.ipad.find((item) => {
                    return item.name === collectionName
                })
            }
        }

        return collection
    }

    getBanners(platform, bannersName) {
        let bannersBox;

        if ( this.state.banners.iphone ) {
            if (platform == "iPhone") {
                bannersBox = this.state.banners.iphone.find((item) => {
                    return item.name === bannersName
                })
            }
            if (platform == "iPad") {
                bannersBox = this.state.banners.ipad.find((item) => {
                    return item.name === bannersName
                })
            }            
        }       
        
        return bannersBox
    }

    changePlatform(platform) {
        this.setState( {activePlatform: platform} )
    }    

    render() {
        return(
            <div className="main-content inner">
                <div className="buttons-box">
                    <Button.Group>
                        <Button
                            className={ classNameBtn("iPhone", this.state.activePlatform) }
                            onClick={() => this.changePlatform("iPhone")}>{"iPhone"}</Button>
                        <Button.Or />
                        <Button 
                            className={ classNameBtn("iPad", this.state.activePlatform) }
                            onClick={() => this.changePlatform("iPad")}>{"iPad"}</Button>
                    </Button.Group>
                </div>

                <div className="main-content__center">
                    <div className="leftside">   
                        <CollectionBox collection={this.getCollection(this.state.activePlatform, COLLECTION_NAMES.new_interesting)} />
                        <BannersBox banners={this.getBanners(this.state.activePlatform, BANNERSBOX_NAMES.popular_collections)}/>
                        <CollectionBox collection={this.getCollection(this.state.activePlatform, COLLECTION_NAMES.classic)} />
                        <CollectionBox collection={this.getCollection(this.state.activePlatform, COLLECTION_NAMES.you_like)} />
                    </div>
                    <Sidebar activePlatform={this.state.activePlatform}/>
                </div>
            </div>
        )
    }
}

export default MainContent;
