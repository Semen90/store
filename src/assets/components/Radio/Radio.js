import React from 'react'
import { Button } from 'semantic-ui-react'

class Radio extends React.Component {
    constructor(props) {
        super(props)

        this.setRadio = this.setRadio.bind(this)
    }

    setRadio(e) {
        const radioParam = 1;

        switch(e.target.name) {
            case "firstBtn":
                radioParam = 1;
                break;
            case "secondBtn":
                radioParam = 2;
                break;

            default:
                radioParam = 1;
        }

        this.props.changePlatform(radioParam);
    }


    render() {
        return (
            <div className="buttons-box">
                <Button.Group>
                    <Button 
                        name="firstBtn"
                        ref={(btn) => this.firstBtn = btn }
                        onClick={this.setRadio}>{this.props.buttons[0]}</Button>
                    <Button.Or />
                    <Button 
                        name="secondBtn"
                        ref={(btn) => this.secndBtn = btn }
                        onClick={this.setRadio}>{this.props.buttons[1]}</Button>
                </Button.Group>
            </div>
        )
    }
}

export default Radio