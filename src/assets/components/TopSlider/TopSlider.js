import React from 'react'
import Slider from 'react-slick'
import { Button } from 'semantic-ui-react'
import axios from 'axios'
import './topSlider.scss'

class TopSlider extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            topBanners: []
        }

        this.prevSlide = this.prevSlide.bind(this)
        this.nextSlide = this.nextSlide.bind(this)
        this.renderSlides = this.renderSlides.bind(this)
    }

    prevSlide() {
        this.slider.slickPrev();
    }

    nextSlide() {
        this.slider.slickNext();
    }

    componentWillMount() {
        axios.get('/getBannersTop')
            .then((response) => {
                this.setState({ ...this.state, topBanners: response.data })
            })
            .catch((err) => {
                console.error(err);
            })
    }

    renderSlides() {
        let slidesList = this.state.topBanners.map((slide, index) => {
            return (
                <div className="top-slider__itemwrap" key={index}>
                    <a href={slide.link} className="top-slider__item" style={{backgroundImage: "url("+ slide.imgLink +")"}}>1</a>
                </div>
            )
        })

        return slidesList;
    }

    render() {

        const settings = {
            centerMode: true,
            centerPadding: '275px',
            slidesToShow: 1,
            infinite: true,
            dots: false,
            speed: 1000,
            autoplay: true,
            autoplaySpeed: 3000,

            // variableWidth: true,
            
            // prevArrow: PrevNavButton,
            // nextArrow: NextNavButton,
            // init: () => {
            //     this.slider.initialSlide(2)
            // },
        };
        
        return (
            <div className="top-slider">
                <Button onClick={this.prevSlide} className="top-slider__arrow _left" circular icon='chevron left' />
                <Button onClick={this.nextSlide} className="top-slider__arrow _right" circular icon='chevron right' />
                <Slider ref={s => this.slider = s} { ...settings }>
                    {this.renderSlides()}
                </Slider>
            </div>
        ) 
    }
}

export default TopSlider;