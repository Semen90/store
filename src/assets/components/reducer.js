import { combineReducers } from 'redux';
import { actionsMain } from './actions';
import { routerReducer } from 'react-router-redux'

const initialState = {
    showModal: false,
    modalData: {}
}

const mainReducer = (state = initialState, action) => {
    switch(action.type) {
        case actionsMain.OPEN_MODAL:
            return {...state, showModal: true, modalData: action.payload}
        case actionsMain.CLOSE_MODAL:
            return {...state, showModal: false}
        default:
            return state;
    }
}

const reducer = combineReducers({
    modal: mainReducer,
    router: routerReducer
});

export default reducer;