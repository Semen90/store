import React from 'react'
import { connect } from 'react-redux'
import { closeModal } from '../actions'
import '../CollectionBox/collectionBox.scss'
import './modal.scss'

class Modal extends React.Component {
    constructor(props) {
        super(props)

        this.renderApps = this.renderApps.bind(this)
        // this.closeModal = this.closeModal.bind(this)
    }

    renderApps() {
        const apps = this.props.collection.apps.map((app, index) => {
            return (
                <li className="apps" key={index}>
                    <a className="apps__img-wrap" href={app.link}>
                        <img src={app.icon} alt="" />
                    </a>
                    <a className="apps__title" href={app.link}>{app.title}</a>
                    <a className="apps__category" href={app.categoryLink}>{app.category}</a>
                    <div className="apps__price">{app.cost > 0 ? app.cost + " $" : "Бесплатно"}</div>
                </li>
            )
        })

        return apps
    }

    closeModal(e) {
        if (e.currentTarget === this.modalCross ) {
            this.props.closeModal();
        }
        
        if (e.target === this.modalClose ) {
            this.props.closeModal();            
        }
    }

    render() {
        return(
            <div className="modal" onClick={this.closeModal.bind(this)} ref={modal => {this.modalClose = modal}}>
                <div className="modal__inner">
                    <div className="modal__title">{this.props.collection.title}</div>
                    <div className="modal__cross" onClick={this.closeModal.bind(this)} ref={modal => {this.modalCross = modal}}>
                        <i className="remove icon"></i>
                    </div>
                    <div className="modal__content">
                        <ul className="collection__list _grid">
                            {this.renderApps()}
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}

const mapState = (state) => {
    return {
        collection: state.modal.modalData
    }
}

const mapDispatch = (dispatch) => {
    return {
        closeModal: () => {
            dispatch(closeModal())
        }
    }
}

export default connect(mapState, mapDispatch)(Modal)
