import React from 'react';
import { connect } from 'react-redux'

import Header from '../Header/Header';
import MainContent from '../MainContent/MainContent';
import Footer from '../Footer/Footer';

import TopSlider from '../TopSlider/TopSlider'
import Modal from '../Modal/Modal'

class Wrapper extends React.Component {
    constructor(props) {
        super(props)

        this.renderModal = this.renderModal.bind(this)
    }


    renderModal() {
        if(this.props.isModalOpen) {
            return <Modal />
                
        }
    }
    
    render() {
        return(
            <div className="wrapper">
                {this.renderModal()}
                {/* <Header /> */}
                <TopSlider />
                <MainContent />
                {/* <Footer /> */}
            </div>
        );
    }
}

const mapState = (state) => {
    return {
        isModalOpen: state.modal.showModal
    }
}

export default connect(mapState)(Wrapper);