export const actionsMain = {
    OPEN_MODAL: 'OPEN_MODAL',
    CLOSE_MODAL: 'CLOSE_MODAL' 
} 

export const openModal = (collectionData) => {
    return { type: actionsMain.OPEN_MODAL, payload: collectionData }
}

export const closeModal = () => {
    return { type: actionsMain.CLOSE_MODAL }
}