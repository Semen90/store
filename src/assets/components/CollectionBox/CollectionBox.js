import React from 'react'
import { connect } from 'react-redux'
import { Button, Icon, Loader } from 'semantic-ui-react'
import { openModal } from '../actions'
import './collectionBox.scss'


class CollectionBox extends React.Component {
    constructor(props) {
        super(props)

        this.renderItems = this.renderItems.bind(this)
        this.openModal = this.openModal.bind(this)    
    }

    renderItems() {
        const itemsList = this.props.collection.apps.map((item, index) => {
            if (index < 12) {
                return (
                    <li className="apps" key={index}>
                        <a className="apps__img-wrap" href={item.link}>
                            <img src={item.icon} alt="" />
                        </a>
                        <a className="apps__title" href={item.link}>{item.title}</a>
                        <a className="apps__category" href={item.categoryLink}>{item.category}</a>
                        <div className="apps__price">{item.cost > 0 ? item.cost + " $" : "Бесплатно"}</div>
                    </li>
                )
            }            
        })
        return itemsList;
    }

    openModal() {
        this.props.openModal(this.props.collection);
    }

    render() {
        const p_ = this.props;

        if (p_.collection) {
            return (
                <div className="collection">
                    <div className="collection__top">
                        <div className="collection__name">{p_.collection.title}</div>
                        <Button onClick={this.openModal} icon labelPosition='right'>
                            См. всё
                            <Icon name='right arrow' />
                        </Button>
                    </div>
                    <ul className="collection__list">
                        {this.renderItems()}
                    </ul>
                </div>
            )
        } else {
            return (
                <div className="collection"></div>
            )
        }
    }
}

const mapState = (state) => {
    return {}
}

const mapDispatch = (dispatch) => {
    return {
        openModal: (collectionData) => {
            dispatch( openModal(collectionData) )
        }
    }
}

export default connect(mapState, mapDispatch)(CollectionBox);