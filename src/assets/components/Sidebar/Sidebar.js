import React from 'react'
import axios from 'axios'
import { Loader } from 'semantic-ui-react' 
import './sidebar.scss'

class Sidebar extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            tops: undefined
        }

        this.renderTop = this.renderTop.bind(this)
    }

    componentWillMount() {
        axios.get('/getTop')
            .then((response) => {
                this.setState( {tops: response.data} )
            })
            .catch((err) => {
                console.error(err)
            })
    }

    renderTop(cost) {
        let collection;

       
        if (this.props.activePlatform === "iPhone") {
            switch(cost) {
                case "free":
                    collection = this.state.tops.free.iphone;
                    break;
    
                case "notfree":
                    collection = this.state.tops.notfree.iphone;
                    break;
    
                default:
                    collection = this.state.tops.free.iphone;
                    break;
            }
        }

        if (this.props.activePlatform === "iPad") {
            switch(cost) {
                case "free":
                    collection = this.state.tops.free.ipad;
                    break;
    
                case "notfree":
                    collection = this.state.tops.notfree.ipad;
                    break;
    
                default:
                    collection = this.state.tops.free.ipad;
                    break;
            }
        }

        const list = collection.map((item, index) => {
            if(index === 0) {
                return (
                    <li key={index} className="app-top">
                        <span className="app-top__index">{(index + 1) + ". "}</span> 
                        <span className="app-top__inner _flex">
                            <a href={item.link} className="app-top__img-wrap">
                                <img src={item.icon} alt="" className="app-top__img" />
                            </a>
                            <span className="__db">
                                <a href={item.link} className="app-top__title">{item.title}</a>
                                <a href={item.categoryLink} className="app-top__category">{item.category}</a>
                            </span>
                        </span>
                        
                    </li>   
                )
            } else {
                return (
                    <li key={index} className="app-top">
                        <span className="app-top__index">{(index + 1) + ". "}</span> 
                        <span className="app-top__inner">
                            <a href={item.link} className="app-top__title">{item.title}</a>
                            <a href={item.categoryLink} className="app-top__category">{item.category}</a>
                        </span>
                    </li>
                )
            }            
        })

        return list
    }

    render() {
        if(this.state.tops) {
            return (
                <div className="sidebar">
                    <div className="sidebar__title">Топ платных приложений</div>
                    <ol className="app-top-list">
                        {this.renderTop("notfree")}
                    </ol>
                    <div className="sidebar__title">Топ бесплатных приложений</div>
                    <ol className="app-top-list">
                        {this.renderTop("free")}
                    </ol>                    
                </div>
            )
        } else {
            return(
                <div className="sidebar">
                    <Loader active inline='centered' />
                </div>
            )
        }        
    }
}

export default Sidebar