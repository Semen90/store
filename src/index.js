import React from 'react';
import ReactDOM from 'react-dom';
import mainCSS from './assets/style/main.scss';
import './assets/images/favicon.ico';
// import 'semantic-ui-css/semantic.min'

import thunkMiddleware from 'redux-thunk';
import { createLogger  } from 'redux-logger';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';

import { BrowserRouter } from 'react-router-dom';
import createHistory from 'history/createBrowserHistory'
import { ConnectedRouter, routerMiddleware, push } from 'react-router-redux'

import Wrapper from './assets/components/Wrapper/Wrapper';
import reducer from './assets/components/reducer';

const history = createHistory();
const rMiddleware = routerMiddleware(history);
const loggerMiddleware = createLogger();

const store = createStore( reducer, applyMiddleware(thunkMiddleware, /*loggerMiddleware,*/ rMiddleware) );

class App extends React.Component {  
    render() {
        return (
            <Wrapper />
        )
    }
}

ReactDOM.render(
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <App />
        </ConnectedRouter>
    </Provider>,
    document.getElementById('app')
);
